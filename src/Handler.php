<?php

namespace Smorken\Errors;

use Illuminate\Http\Response;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\ViewErrorBag;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Throwable;

class Handler extends \Illuminate\Foundation\Exceptions\Handler
{
    public function getDontFlash(): array
    {
        return $this->dontFlash;
    }

    protected function getLayoutComponentName(): string
    {
        return Config::get('errors.layout', 'layouts.app');
    }

    protected function registerErrorViewPaths(): void
    {
        (new RegisterErrorViewPaths)();
    }

    protected function renderHttpException(HttpExceptionInterface $e): Response|SymfonyResponse
    {
        $this->registerErrorViewPaths();

        if ($view = $this->getHttpExceptionView($e)) {
            try {
                return response()->view($view, [
                    'errors' => new ViewErrorBag,
                    'exception' => $e,
                    'layoutComponent' => $this->getLayoutComponentName(),
                ], $e->getStatusCode(), $e->getHeaders());
            } catch (Throwable $t) {
                config('app.debug') && throw $t;

                $this->report($t);
            }
        }

        return $this->convertExceptionToResponse($e);
    }
}
