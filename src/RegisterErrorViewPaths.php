<?php

declare(strict_types=1);

namespace Smorken\Errors;

use Illuminate\Support\Facades\View;

class RegisterErrorViewPaths
{
    public function __invoke(): void
    {
        View::replaceNamespace('errors', collect(config('view.paths'))->map(fn ($path) => "{$path}/errors")->push(
            __DIR__.'/../views',
            $this->getLaravelErrorViewPath()
        )->all());
    }

    protected function getLaravelErrorViewPath(): string
    {
        $rc = new \ReflectionClass(\Illuminate\Foundation\Exceptions\RegisterErrorViewPaths::class);

        return dirname($rc->getFileName()).'/views';
    }
}
