<?php

declare(strict_types=1);

namespace Smorken\Errors\Contracts;

use Illuminate\Foundation\Configuration\Exceptions;
use Smorken\Errors\Constants\DefaultMethod;

interface ExceptionsHelper
{
    public function apply(): void;

    public function excludeMethod(DefaultMethod $method): self;

    public function includeMethod(DefaultMethod $method): self;

    public function withLayout(string $layout): self;

    public static function create(Exceptions $exceptions): self;
}
