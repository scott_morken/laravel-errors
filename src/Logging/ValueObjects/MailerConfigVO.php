<?php

declare(strict_types=1);

namespace Smorken\Errors\Logging\ValueObjects;

use Illuminate\Contracts\Config\Repository;
use Illuminate\Contracts\Mail\Mailer;

class MailerConfigVO
{
    public function __construct(
        public ?Mailer $mailer,
        public Repository $repository,
        public string|array|null $to,
        public string $subjectFormat = '[%datetime%] %level_name%: %message%'
    ) {}
}
