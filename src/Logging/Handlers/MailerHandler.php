<?php

declare(strict_types=1);

namespace Smorken\Errors\Logging\Handlers;

use Illuminate\Mail\Message;
use Illuminate\Support\Str;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\MailHandler;
use Monolog\Level;
use Monolog\LogRecord;
use Smorken\Errors\Logging\ValueObjects\MailerConfigVO;

class MailerHandler extends MailHandler
{
    protected ?Message $lastMessage = null;

    public function __construct(
        protected MailerConfigVO $mailerConfig,
        int|string|Level $level = Level::Debug,
        bool $bubble = true
    ) {
        parent::__construct($level, $bubble);
    }

    public function getLastMessage(): ?Message
    {
        return $this->lastMessage;
    }

    protected function getFrom(): string
    {
        return $this->mailerConfig->repository->get('mail.from.address');
    }

    /**
     * @phpstan-param non-empty-array<LogRecord> $records
     */
    protected function getSubject(array $records): string
    {
        $formatter = new LineFormatter($this->mailerConfig->subjectFormat);

        return Str::limit($formatter->format($this->getHighestRecord($records)), 250);
    }

    protected function send(string $content, array $records): void
    {
        $to = $this->mailerConfig->to;
        if (! $to) {
            return;
        }
        try {
            $this->mailerConfig->mailer?->send([], [],
                function (Message $message) use ($content, $records, $to) {
                    $message->to($to)
                        ->subject($this->getSubject($records))
                        ->from($this->getFrom())
                        ->html($content);
                    $this->lastMessage = $message;
                });
        } catch (\Throwable $e) {
            echo $e->getMessage();
        }
    }
}
