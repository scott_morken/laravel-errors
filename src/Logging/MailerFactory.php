<?php

declare(strict_types=1);

namespace Smorken\Errors\Logging;

use Monolog\Formatter\HtmlFormatter;
use Monolog\Level;
use Monolog\Logger;
use Psr\Log\LoggerInterface;
use Smorken\Errors\Logging\Handlers\MailerHandler;
use Smorken\Errors\Logging\ValueObjects\MailerConfigVO;

class MailerFactory
{
    protected array $config = [
        'level' => Level::Error,
        'bubble' => true,
    ];

    public function __construct(
        protected MailerConfigVO $mailerConfig
    ) {}

    public function __invoke(array $config): LoggerInterface
    {
        if (isset($config['level'])) {
            $config['level'] = Logger::toMonologLevel($config['level']);
        }
        $this->config = array_merge($this->config, $config);
        $handler = new MailerHandler(
            $this->mailerConfig,
            $this->fromConfig('level'),
            $this->fromConfig('bubble')
        );
        $handler->setFormatter(new HtmlFormatter);

        return new Logger('mailer', [$handler]);
    }

    protected function fromConfig(string $key, mixed $default = null): mixed
    {
        return $this->config[$key] ?? $default;
    }
}
