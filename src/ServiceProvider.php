<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 2/12/15
 * Time: 8:40 AM
 */

namespace Smorken\Errors;

use Illuminate\Contracts\Config\Repository;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Mail\Mailer;
use Smorken\Errors\Logging\MailerFactory;
use Smorken\Errors\Logging\ValueObjects\MailerConfigVO;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function boot(): void
    {
        $this->bootConfig();
        $this->publishViews();
        $loadroutes = $this->app['config']->get('errors.routes.enabled', false);
        if ($loadroutes) {
            $this->loadRoutes();
        }
    }

    public function register(): void
    {
        $this->registerMailerFactory();
    }

    protected function bootConfig(): void
    {
        $config = realpath(__DIR__.'/../config/config.php');
        $this->mergeConfigFrom($config, 'errors');
        $this->publishes([$config => config_path('sm-errors.php')], 'errors-config');
    }

    protected function loadRoutes(): void
    {
        $routefile = __DIR__.'/../routes/routes.php';
        if (file_exists($routefile)) {
            $prefix = $this->app['config']->get('errors.routes.prefix', 'error');
            include $routefile;
        }
    }

    protected function publishViews(): void
    {
        $path = realpath(__DIR__.'/../views');
        $this->publishes([
            $path => resource_path('/views/errors'),
        ], 'errors-views');
    }

    protected function registerMailerFactory(): void
    {
        $this->app->scoped(MailerFactory::class, function (Application $app) {
            $mailMailer = $app['config']->get('mail.default');
            $mailer = $mailMailer === 'log' ? null : $app[Mailer::class];
            $repository = $app[Repository::class];
            $to = $repository->get('errors.email_to');
            $mailerConfig = new MailerConfigVO(
                $mailer,
                $repository,
                $to
            );

            return new MailerFactory($mailerConfig);
        });
    }
}
