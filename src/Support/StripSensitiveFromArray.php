<?php

declare(strict_types=1);

namespace Smorken\Errors\Support;

class StripSensitiveFromArray
{
    public static array $fullKeys = [];

    public static array $keyContains = [
        'pass',
        'key',
        'token',
        'salt',
        'cookie',
        'secret',
    ];

    public function __construct(protected array $from) {}

    public function strip(): array
    {
        $stripped = $this->removeKeyMatches($this->from, self::$fullKeys);

        return $this->removeKeyMatches($stripped, self::$keyContains);
    }

    protected function removeKeyMatches(array $from, array $keys): array
    {
        $stripped = [];
        foreach ($from as $k => $v) {
            $hasPartialMatch = false;
            foreach ($keys as $contain) {
                if (stripos($k, (string) $contain) !== false) {
                    $hasPartialMatch = true;
                    break;
                }
            }
            if (! $hasPartialMatch) {
                $stripped[$k] = $v;
            }
        }

        return $stripped;
    }
}
