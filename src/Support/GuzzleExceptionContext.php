<?php

declare(strict_types=1);

namespace Smorken\Errors\Support;

use GuzzleHttp\Exception\RequestException;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

final class GuzzleExceptionContext
{
    public function __construct(protected \Throwable $e) {}

    public static function fromException(\Throwable $e): self
    {
        return new self($e);
    }

    public function toArray(): array
    {
        if ($this->e instanceof RequestException) {
            return [
                'httpRequest' => $this->exceptionContextForGuzzleRequest($this->e->getRequest()),
                'httpResponse' => $this->exceptionContextForGuzzleResponse($this->e->getResponse()),
            ];
        }

        return [];
    }

    private function exceptionContextForGuzzleRequest(RequestInterface $request): array
    {
        return [
            'headers' => $request->getHeaders(),
            'target' => $request->getRequestTarget(),
            'method' => $request->getMethod(),
            'body' => (string) $request->getBody(),
        ];
    }

    private function exceptionContextForGuzzleResponse(ResponseInterface $response): array
    {
        return [
            'headers' => $response->getHeaders(),
            'statusCode' => $response->getStatusCode(),
            'body' => (string) $response->getBody(),
        ];
    }
}
