<?php

declare(strict_types=1);

namespace Smorken\Errors\Support;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;

final class RenderTokenMismatch
{
    public function __construct(
        protected TokenMismatchException $e,
        protected Request $request,
        protected array $dontFlash
    ) {}

    public static function fromException(TokenMismatchException $e, Request $request, array $dontFlash): self
    {
        return new self($e, $request, $dontFlash);
    }

    public function render(): JsonResponse|RedirectResponse
    {
        return $this->handleTokenMisMatch();
    }

    private function handleTokenMisMatch(): JsonResponse|RedirectResponse
    {
        if ($this->request->expectsJson()) {
            return Response::json(['error' => true, 'message' => 'Token mismatch', 'code' => 419], 419);
        }

        return Redirect::back()
            ->withInput($this->request->except([...$this->dontFlash, '_token']))
            ->withErrors('Your form has expired, please try again.');
    }
}
