<?php

declare(strict_types=1);

namespace Smorken\Errors;

use Illuminate\Foundation\Configuration\Exceptions;
use Illuminate\Http\Exceptions\PostTooLargeException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Session\TokenMismatchException;
use Smorken\Errors\Constants\DefaultMethod;
use Smorken\Errors\Support\GuzzleExceptionContext;
use Smorken\Errors\Support\RenderTokenMismatch;
use Smorken\Errors\Support\StripSensitiveFromArray;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Throwable;

final class ExceptionsHelper implements \Smorken\Errors\Contracts\ExceptionsHelper
{
    protected array $defaultMethods = [
        DefaultMethod::DONT_REPORT_DUPLICATES->value => true,
        DefaultMethod::MAP_POST_TOO_LARGE_EXCEPTION->value => true,
        DefaultMethod::ADD_REQUEST_CONTEXT->value => true,
        DefaultMethod::ADD_GUZZLE_CONTEXT->value => true,
        DefaultMethod::ADD_SERVER_CONTEXT->value => true,
        DefaultMethod::RENDER_TOKEN_MISMATCH_EXCEPTION->value => true,
        DefaultMethod::ADD_ENV_CONTEXT->value => false,
    ];

    protected string $layout = 'layouts.app';

    public function __construct(
        protected Exceptions $exceptions,
    ) {}

    public static function create(Exceptions $exceptions): Contracts\ExceptionsHelper
    {
        return new self($exceptions);
    }

    public function apply(): void
    {
        foreach ($this->defaultMethods as $method => $active) {
            $active && $this->{$method}();
        }
    }

    public function excludeMethod(DefaultMethod $method): Contracts\ExceptionsHelper
    {
        $this->defaultMethods[$method->value] = false;

        return $this;
    }

    public function includeMethod(DefaultMethod $method): Contracts\ExceptionsHelper
    {
        $this->defaultMethods[$method->value] = true;

        return $this;
    }

    public function withLayout(string $layout): Contracts\ExceptionsHelper
    {
        $this->layout = $layout;

        return $this;
    }

    protected function addEnvContext(): void
    {
        $strip = new StripSensitiveFromArray($_ENV);
        $this->exceptions->context(fn () => [
            '_ENV' => $strip->strip(),
        ]);
    }

    protected function addGuzzleContext(): void
    {
        $this->exceptions->context(fn (Throwable $exception): array => GuzzleExceptionContext::fromException($exception)
            ->toArray());
    }

    protected function addRequestContext(): void
    {
        $strip = new StripSensitiveFromArray($_REQUEST);
        $this->exceptions->context(fn () => [
            '_REQUEST' => $strip->strip(),
        ]);
    }

    protected function addServerContext(): void
    {
        $strip = new StripSensitiveFromArray($_SERVER);
        $this->exceptions->context(fn () => [
            '_SERVER' => $strip->strip(),
        ]);
    }

    protected function dontFlashToken(): void
    {
        $this->exceptions->dontFlash(['_token']);
    }

    protected function dontReportDuplicates(): void
    {
        $this->exceptions->dontReportDuplicates();
    }

    protected function mapPostTooLargeException(): void
    {
        $this->exceptions->map(PostTooLargeException::class,
            fn (PostTooLargeException $e): HttpException => new HttpException(413, $e->getMessage(), $e));
    }

    protected function renderTokenMisMatchException(): void
    {
        $this->exceptions->render(function (HttpException $e, Request $request): JsonResponse|RedirectResponse|null {
            $inner = $e->getPrevious();
            if (! is_a($inner, TokenMismatchException::class)) {
                return null;
            }

            // @phpstan-ignore method.notFound
            return RenderTokenMismatch::fromException($inner, $request, $this->exceptions->handler->getDontFlash())
                ->render();
        });
    }
}
