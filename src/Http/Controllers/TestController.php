<?php

namespace Smorken\Errors\Http\Controllers;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class TestController extends Controller
{
    public function exception(Request $request): void
    {
        throw new \Exception('Test Exception');
    }

    public function token(Request $request): Response
    {
        $data = [
            'errors' => $request->session()->get('errors', []),
            'old' => $request->old('foo', null),
        ];
        $html = '<html><body><pre>'.\Smorken\Support\Arr::stringify($data).'</pre>'.
            '<form method="post"><input name="foo" value=""><button type="submit">Submit</button></form></body>';

        return \Illuminate\Support\Facades\Response::make($html);
    }

    public function doToken(Request $request): void
    {
        throw new \Illuminate\Session\TokenMismatchException('Test Token Exception.');
    }

    public function error(Request $request): void
    {
        trigger_error('Test Error.', E_USER_ERROR);
    }

    public function notFound(Request $request): void
    {
        throw new NotFoundHttpException('Nothing here.');
    }

    public function notAuthorized(Request $request): void
    {
        throw new AuthorizationException('Not authorized.');
    }

    public function validation(Request $request): void
    {
        throw ValidationException::withMessages(['error' => ['Validation error.']]);
    }
}
