<?php

declare(strict_types=1);

namespace Smorken\Errors\Constants;

enum DefaultMethod: string
{
    case DONT_REPORT_DUPLICATES = 'dontReportDuplicates';
    case MAP_POST_TOO_LARGE_EXCEPTION = 'mapPostTooLargeException';

    case ADD_REQUEST_CONTEXT = 'addRequestContext';

    case ADD_SERVER_CONTEXT = 'addServerContext';

    case ADD_ENV_CONTEXT = 'addEnvContext';

    case ADD_GUZZLE_CONTEXT = 'addGuzzleContext';

    case RENDER_TOKEN_MISMATCH_EXCEPTION = 'renderTokenMismatchException';
}
