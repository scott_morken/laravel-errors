## Error extension for Laravel

### License

This software is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

### Installation

Add as the first service provider to your config/app.php

    ...
    'providers' => [
            Smorken\Errors\ErrorServiceProvider::class,
    ...
    
Publish the files

```
php artisan vendor:publish --provider="Smorken\Errors\ErrorServiceProvider"
```

This will provide a `resource/views/errors` directory and `config/errors.php`

Modify any of the settings you wish (email to address, layout/master view)

The view files expect a variable called `$master` to be available with the name of the master view to extend.
(eg, `view()->share('master', 'layouts.master')`)
