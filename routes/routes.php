<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 2/18/15
 * Time: 7:04 AM
 */

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Route;

Route::group([
    'prefix' => ($prefix ?? Config::get('errors.routes.prefix', 'error')),
    'middleware' => 'web',
], function () {
    $controller = \Smorken\Errors\Http\Controllers\TestController::class;
    Route::get('/error', $controller.'@error');
    Route::get('/exception', $controller.'@exception');
    Route::get('/token', $controller.'@token');
    Route::post('/token', $controller.'@doToken');
    Route::get('/validation', $controller.'@validation');
    Route::get('/404', $controller.'@notFound');
    Route::get('/403', $controller.'@notAuthorized');
});
