<?php

return [
    'layout' => env('ERRORS_LAYOUT_COMPONENT', 'layouts.app'),
    'routes' => [
        'enabled' => env('ERRORS_ROUTES_ENABLED', false),
        'prefix' => 'error',
    ],
    'email_to' => env('ERROR_EMAIL'),
];
