<?php

declare(strict_types=1);

namespace Tests\Smorken\Errors\Concerns;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Contracts\Container\Container;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Events\Dispatcher;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Facades\Facade;
use Illuminate\View\Engines\EngineResolver;
use Illuminate\View\FileViewFinder;
use Mockery;

trait WithApplication
{
    protected Application|Container|null $app = null;

    protected function getApp(): Application
    {
        if (! $this->app) {
            $app = new \Illuminate\Foundation\Application(realpath(__DIR__.'/../../'));
            \Illuminate\Container\Container::setInstance($app);
            Facade::clearResolvedInstances();
            Facade::setFacadeApplication($app);
            $this->app = $app;
        }

        return $this->app;
    }

    protected function setConfigRepository(Repository|array $config): void
    {
        if (is_array($config)) {
            $config = new \Illuminate\Config\Repository($config);
        }
        $this->getApp()->bind('config', fn () => $config);
        $this->getApp()->bind(Repository::class, fn () => $config);
    }

    protected function setGuard(?Guard $guard = null): void
    {
        if (! $guard) {
            $guard = Mockery::mock(Guard::class);
            $guard->shouldReceive('user')->andReturn(new User(['id' => 1]));
            $guard->shouldReceive('hasUser')->andReturn(true);
            $guard->shouldReceive('id')->andReturn(1);
        }
        $this->getApp()->bind(Guard::class, fn () => $guard);
    }

    protected function setView(?Factory $view = null): void
    {
        if (! $view) {
            $view = new \Illuminate\View\Factory(
                new EngineResolver,
                new FileViewFinder(new Filesystem, [
                    __DIR__.'/../views',
                ]),
                new Dispatcher
            );
        }
        $this->getApp()->bind(Factory::class, fn () => $view);
        $this->getApp()->bind('view', fn () => $view);
    }
}
