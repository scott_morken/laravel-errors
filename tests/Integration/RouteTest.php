<?php

namespace Tests\Smorken\Errors\Integration;

use Tests\Smorken\Errors\BaseTestCase;

class RouteTest extends BaseTestCase
{
    public function test403(): void
    {
        $r = $this->call('GET', 'error/403');
        $content = $r->getContent();
        $this->assertStringContainsString('Sorry, you don\'t have access.', $content);
        $this->assertStringContainsString('Not authorized.', $content);
    }

    public function test404(): void
    {
        $r = $this->call('GET', 'error/404');
        $content = $r->getContent();
        $this->assertStringContainsString('I don\'t know where to find that.', $content);
        $this->assertStringContainsString('Nothing here.', $content);
    }

    public function testErrorRoute(): void
    {
        $r = $this->call('GET', 'error/error');
        $content = $r->getContent();
        $this->assertStringContainsString('Whoops, that didn\'t work!', $content);
        $this->assertStringNotContainsString('Test Error', $content);
    }

    public function testExceptionRoute(): void
    {
        $r = $this->call('GET', 'error/exception');
        $content = $r->getContent();
        $this->assertStringContainsString('Whoops, that didn\'t work!', $content);
        $this->assertStringNotContainsString('Test Exception', $content);
    }

    public function testExceptionRouteExceptionInContextHandlesFirstException(): void
    {
        $this->app->bind(\Illuminate\Contracts\Auth\Guard::class, function () {
            throw new \Exception('Auth exception.');
        });
        $r = $this->call('GET', 'error/exception');
        $content = $r->getContent();
        $this->assertStringContainsString('Whoops, that didn\'t work!', $content);
        $this->assertStringNotContainsString('Test Exception', $content);
    }

    public function testFormWithInvalidToken(): void
    {
        $this->get('/error/token'); // prep redirect uri
        $r = $this->post('/error/token', ['bar' => 'foo']);
        $r->assertRedirect('/error/token');
        $r->assertSessionHasInput('bar', 'foo');
        $errors = $r->getSession()->get('errors');
        $this->assertEquals([0 => ['Your form has expired, please try again.']], $errors->toArray());
    }
}
