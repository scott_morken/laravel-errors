<?php

namespace Tests\Smorken\Errors;

use Illuminate\Contracts\Config\Repository;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Foundation\Configuration\Exceptions;
use Orchestra\Testbench\TestCase;
use Smorken\Errors\ExceptionsHelper;
use Smorken\Errors\Handler;

use function Orchestra\Testbench\after_resolving;

abstract class BaseTestCase extends TestCase
{
    public $baseUrl = 'http://localhost';

    protected function defineEnvironment($app)
    {
        tap($app['config'], function (Repository $config) {
            $config->set('app.env', 'production');
            $config->set('app.debug', false);
            $config->set('mail.driver', 'array');
            $config->set('errors.routes.enabled', true);
            $config->set('errors.layout', 'errors::layout');
        });
    }

    protected function getPackageProviders($app)
    {
        return [
            \Smorken\Errors\ServiceProvider::class,
        ];
    }

    protected function resolveApplicationExceptionHandler($app)
    {
        $app->singleton('Illuminate\Contracts\Debug\ExceptionHandler', Handler::class);
    }

    protected function setUp(): void
    {
        // Code before application created.

        $this->afterApplicationCreated(function () {
            after_resolving($this->app, ExceptionHandler::class, function (ExceptionHandler $handler) {
                $exceptions = new Exceptions($handler);
                ExceptionsHelper::create($exceptions)->apply();
            });
        });

        $this->beforeApplicationDestroyed(function () {
            // Code before application destroyed.
        });

        parent::setUp();
    }
}
