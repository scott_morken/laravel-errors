<?php

declare(strict_types=1);

namespace Tests\Smorken\Errors\Unit\Logging;

use Illuminate\Config\Repository;
use Illuminate\Contracts\Mail\Mailer;
use Mockery as m;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Smorken\Errors\Logging\Handlers\MailerHandler;
use Smorken\Errors\Logging\MailerFactory;
use Smorken\Errors\Logging\ValueObjects\MailerConfigVO;

class MailerFactoryTest extends TestCase
{
    protected ?Mailer $mailer = null;

    #[Test]
    public function it_creates_a_logger_with_a_mailer_handler(): void
    {
        $sut = $this->getSut($this->getMailerConfigVO([], null));
        $logger = $sut([]);
        $handlers = $logger->getHandlers();
        $this->assertInstanceOf(LoggerInterface::class, $logger);
        $this->assertCount(1, $handlers);
        $this->assertInstanceOf(MailerHandler::class, $handlers[0]);
    }

    protected function getMailer(): Mailer
    {
        return $this->mailer ??= m::mock(Mailer::class);
    }

    protected function getMailerConfigVO(array $config, array|string|null $to): MailerConfigVO
    {
        $repo = new Repository($config);

        return new MailerConfigVO($this->getMailer(), $repo, $to);
    }

    protected function getSut(MailerConfigVO $config): MailerFactory
    {
        return new MailerFactory($config);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
