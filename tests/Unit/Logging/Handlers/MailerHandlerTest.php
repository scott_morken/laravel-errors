<?php

declare(strict_types=1);

namespace Tests\Smorken\Errors\Unit\Logging\Handlers;

use Illuminate\Config\Repository;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Mail\Message;
use Mockery as m;
use Monolog\Logger;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Smorken\Errors\Logging\Handlers\MailerHandler;
use Smorken\Errors\Logging\ValueObjects\MailerConfigVO;
use Symfony\Component\Mime\Email;

class MailerHandlerTest extends TestCase
{
    protected ?Mailer $mailer = null;

    #[Test]
    public function it_does_not_send_a_message_when_the_mailer_is_null(): void
    {
        $this->getMailer()->expects()
            ->send([], [], m::type(\Closure::class))
            ->never();
        $vo = new MailerConfigVO(null, new Repository([]), 'foo@example.edu');
        $handler = new MailerHandler($vo);
        $logger = new Logger('test', [$handler]);
        $logger->error('Test error', ['foo' => 'bar']);
        $this->assertNull($handler->getLastMessage());
    }

    #[Test]
    public function it_sends_a_message(): void
    {
        $this->getMailer()->expects()->send([], [], m::type(\Closure::class))
            ->andReturnUsing(function ($view, $with, $callback) {
                $callback(new Message(new Email));

                return true;
            });
        $handler = new MailerHandler($this->getMailerConfigVO(['mail' => ['from' => ['address' => 'from@example.edu']]],
            'foo@example.edu'));
        $logger = new Logger('test', [$handler]);
        $logger->error('Test error', ['foo' => 'bar']);
        $sent = $handler->getLastMessage();
        $this->assertStringContainsString('Test error', $sent->getHtmlBody());
        $this->assertStringContainsString('] ERROR: Test error', $sent->getSubject());
    }

    protected function getMailer(): Mailer
    {
        return $this->mailer ??= m::mock(Mailer::class);
    }

    protected function getMailerConfigVO(array $config, array|string|null $to): MailerConfigVO
    {
        $repo = new Repository($config);

        return new MailerConfigVO($this->getMailer(), $repo, $to);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
