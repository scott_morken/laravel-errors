<?php

namespace Tests\Smorken\Errors\Unit;

use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Psr7\Response;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Configuration\Exceptions;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Support\Facades\Redirect;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Smorken\Errors\ExceptionsHelper;
use Smorken\Errors\Handler;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Tests\Smorken\Errors\Concerns\WithApplication;

class HandlerTest extends TestCase
{
    use WithApplication;

    public function testRenderAuthorizationExceptionAsBasicResponse(): void
    {
        $sut = new Handler($this->getApp());
        $this->setConfigRepository($this->getConfig(['app' => ['debug' => false]]));
        $exceptions = new Exceptions($sut);
        ExceptionsHelper::create($exceptions)->apply();
        $request = new Request;
        $response = $sut->render($request, new AuthorizationException('Nope!'));
        $this->assertEquals(403, $response->getStatusCode());
        $this->assertStringContainsString('The server returned a "403 Forbidden".', (string) $response->getContent());
    }

    public function testRenderModelNotFoundExceptionAsBasicResponse(): void
    {
        $sut = new Handler($this->getApp());
        $this->setConfigRepository($this->getConfig(['app' => ['debug' => false]]));
        $exceptions = new Exceptions($sut);
        ExceptionsHelper::create($exceptions)->apply();
        $request = new Request;
        $response = $sut->render($request, new ModelNotFoundException('Not found.'));
        $this->assertEquals(404, $response->getStatusCode());
        $this->assertStringContainsString('The server returned a "404 Not Found".',
            (string) $response->getContent());
    }

    public function testRenderNonHttpExceptionAsAjaxWhenDebugFalse(): void
    {
        $sut = new Handler($this->getApp());
        $this->setConfigRepository($this->getConfig(['app' => ['debug' => false]]));
        $exceptions = new Exceptions($sut);
        ExceptionsHelper::create($exceptions)->apply();
        $request = new Request;
        $request->headers->set('X-Requested-With', 'XMLHttpRequest');
        $response = $sut->render($request, new \Exception('Test'));
        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertEquals(500, $response->getStatusCode());
        $expected = [
            'message' => 'Server Error',
        ];
        $responseArray = json_decode($response->getContent(), true);
        $this->assertEquals($expected, $responseArray);
    }

    public function testRenderNonHttpExceptionAsAjaxWhenDebugTrue(): void
    {
        $sut = new Handler($this->getApp());
        $this->setConfigRepository($this->getConfig(['app' => ['debug' => true]]));
        $exceptions = new Exceptions($sut);
        ExceptionsHelper::create($exceptions)->apply();
        $request = new Request;
        $request->headers->set('X-Requested-With', 'XMLHttpRequest');
        $response = $sut->render($request, new \Exception('Test'));
        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertEquals(500, $response->getStatusCode());
        $expected = [
            'message' => 'Test',
            'exception' => 'Exception',
            'file' => '/app/tests/Unit/HandlerTest.php',
        ];
        $responseArray = json_decode($response->getContent(), true);
        foreach ($expected as $k => $v) {
            $this->assertEquals($v, $responseArray[$k]);
        }
    }

    public function testRenderNotFoundHttpExceptionAsBasicResponse(): void
    {
        $sut = new Handler($this->getApp());
        $this->setConfigRepository($this->getConfig(['app' => ['debug' => false]]));
        $exceptions = new Exceptions($sut);
        ExceptionsHelper::create($exceptions)->apply();
        $request = new Request;
        $response = $sut->render($request, new NotFoundHttpException('Not found.'));
        $this->assertEquals(404, $response->getStatusCode());
        $this->assertStringContainsString('The server returned a "404 Not Found".',
            (string) $response->getContent());
    }

    public function testReportGuzzleClientException(): void
    {
        $container = $this->getApp();
        $logger = m::mock(LoggerInterface::class);
        $container->bind(LoggerInterface::class, function () use ($logger) {
            return $logger;
        });
        $e = new ClientException(
            'Guzzle Error',
            new \GuzzleHttp\Psr7\Request('GET', '/foo', ['header1' => 'headervalue'], 'request body'),
            new Response(500, ['header1' => 'responseheader'], 'response body')
        );
        $sut = new Handler($container);
        $this->setConfigRepository($this->getConfig(['app' => ['debug' => false]]));
        $exceptions = new Exceptions($sut);
        ExceptionsHelper::create($exceptions)->apply();
        $logger->shouldReceive('error')
            ->once()
            ->with('Guzzle Error', m::type('array'))
            ->andReturnUsing(function ($errorString, $context) use ($e) {
                $expectedRequest = [
                    'headers' => [
                        'header1' => ['headervalue'],
                    ],
                    'target' => '/foo',
                    'method' => 'GET',
                    'body' => 'request body',
                ];
                $expectedResponse = [
                    'headers' => [
                        'header1' => ['responseheader'],
                    ],
                    'statusCode' => 500,
                    'body' => 'response body',
                ];
                $this->assertSame($e, $context['exception']);
                $this->assertEquals($expectedRequest, $context['httpRequest']);
                $this->assertEquals($expectedResponse, $context['httpResponse']);
            });
        $sut->report($e);
    }

    public function testReportGuzzleServerException(): void
    {
        $container = $this->getApp();
        $logger = m::mock(LoggerInterface::class);
        $container->bind(LoggerInterface::class, function () use ($logger) {
            return $logger;
        });
        $e = new ServerException(
            'Guzzle Error',
            new \GuzzleHttp\Psr7\Request('GET', '/foo', ['header1' => 'headervalue'], 'request body'),
            new Response(500, ['header1' => 'responseheader'], 'response body')
        );
        $sut = new Handler($container);
        $this->setConfigRepository($this->getConfig(['app' => ['debug' => false]]));
        $exceptions = new Exceptions($sut);
        ExceptionsHelper::create($exceptions)->apply();
        $logger->shouldReceive('error')
            ->once()
            ->with('Guzzle Error', m::type('array'))
            ->andReturnUsing(function ($errorString, $context) use ($e) {
                $expectedRequest = [
                    'headers' => [
                        'header1' => ['headervalue'],
                    ],
                    'target' => '/foo',
                    'method' => 'GET',
                    'body' => 'request body',
                ];
                $expectedResponse = [
                    'headers' => [
                        'header1' => ['responseheader'],
                    ],
                    'statusCode' => 500,
                    'body' => 'response body',
                ];
                $this->assertSame($e, $context['exception']);
                $this->assertEquals($expectedRequest, $context['httpRequest']);
                $this->assertEquals($expectedResponse, $context['httpResponse']);
            });
        $sut->report($e);
    }

    public function testTokenMismatchIsRedirectBack(): void
    {
        $sut = new Handler($this->getApp());
        $this->setConfigRepository($this->getConfig(['app' => ['debug' => false]]));
        $exceptions = new Exceptions($sut);
        ExceptionsHelper::create($exceptions)->apply();
        $request = new Request(['_token' => 'abc123', 'username' => 'foo', 'password' => 'bar']);
        $this->getApp()->bind('request', fn () => $request);
        $e = new TokenMismatchException('Bad token');
        $redirResponse = m::mock(RedirectResponse::class);
        Redirect::setFacadeApplication($this->getApp());
        Redirect::shouldReceive('back')
            ->once()
            ->andReturn($redirResponse);
        $redirResponse->shouldReceive('withInput')
            ->once()
            ->with(['username' => 'foo'])
            ->andReturnSelf();
        $redirResponse->shouldReceive('withErrors')
            ->once()
            ->with('Your form has expired, please try again.')
            ->andReturnSelf();
        $response = $sut->render($request, $e);
        $this->assertSame($redirResponse, $response);
    }

    protected function getConfig(array $config): Repository
    {
        return new \Illuminate\Config\Repository($config);
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->setView();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
