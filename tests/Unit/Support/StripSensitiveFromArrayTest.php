<?php

declare(strict_types=1);

namespace Tests\Smorken\Errors\Unit\Support;

use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Smorken\Errors\Support\StripSensitiveFromArray;

class StripSensitiveFromArrayTest extends TestCase
{
    #[Test]
    public function it_strips_sensitive_full_keys(): void
    {
        StripSensitiveFromArray::$fullKeys = ['HTTP_HOST'];
        $sut = new StripSensitiveFromArray([
            'GPG_KEYS' => 'abc123',
            'HTTP_HOST' => 'localhost',
            'SERVER_NAME' => 'localhost',
            'SERVER_PORT' => '8080',
            'APP_KEY' => 'abc123',
            'DB_PASSWORD' => 'pw',
            'AWS_ACCESS_KEY_ID' => 'abc123',
            'AWS_SECRET_KEY' => 'pw',
            'AZURE_CLIENT_SECRET' => 'abc123',
        ]);
        $this->assertEquals([
            'SERVER_NAME' => 'localhost',
            'SERVER_PORT' => '8080',
        ], $sut->strip());
    }

    #[Test]
    public function it_strips_sensitive_partial_keys(): void
    {
        $sut = new StripSensitiveFromArray([
            'GPG_KEYS' => 'abc123',
            'HTTP_HOST' => 'localhost',
            'SERVER_NAME' => 'localhost',
            'SERVER_PORT' => '8080',
            'APP_KEY' => 'abc123',
            'DB_PASSWORD' => 'pw',
            'AWS_ACCESS_KEY_ID' => 'abc123',
            'AWS_SECRET_KEY' => 'pw',
            'AZURE_CLIENT_SECRET' => 'abc123',
        ]);
        $this->assertEquals([
            'HTTP_HOST' => 'localhost',
            'SERVER_NAME' => 'localhost',
            'SERVER_PORT' => '8080',
        ], $sut->strip());
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        StripSensitiveFromArray::$fullKeys = [];
    }
}
