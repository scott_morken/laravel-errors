@php($layoutComponent = $layoutComponent ?? \Illuminate\Support\Facades\Config::get('sm-errors.layout', 'layouts.app'))
<x-dynamic-component :component="$layoutComponent">
    <div class="exception well">
        <h3>Invalid Request Token</h3>
        <div>
            The request you sent was not properly authorized. Please refresh the original page and try again.
        </div>
    </div>
</x-dynamic-component>
