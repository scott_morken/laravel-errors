@php($layoutComponent = $layoutComponent ?? \Illuminate\Support\Facades\Config::get('sm-errors.layout', 'layouts.app'))
<x-dynamic-component :component="$layoutComponent">
    <div class="exception well">
        <h3>Maintenance</h3>
        <div class="descr">
            The site is currently down for maintenance. We'll be back up as fast as possible!
        </div>
    </div>
</x-dynamic-component>
