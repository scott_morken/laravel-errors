@php($layoutComponent = $layoutComponent ?? \Illuminate\Support\Facades\Config::get('sm-errors.layout', 'layouts.app'))
<x-dynamic-component :component="$layoutComponent">
    <div class="exception well">
        <h3>Unauthorized</h3>
        <div class="descr">
            Please login to access this resource.
            @include('errors::_message', ['message' => isset($exception) ? $exception->getMessage() : null])
        </div>
    </div>
</x-dynamic-component>
