@php($layoutComponent = $layoutComponent ?? \Illuminate\Support\Facades\Config::get('sm-errors.layout', 'layouts.app'))
<x-dynamic-component :component="$layoutComponent">
    <div class="exception well">
        <h3>POST too large</h3>
        <div>
            The data you are trying to send to the server is too large.  Check your file upload size
            if you are uploading files.
        </div>
    </div>
</x-dynamic-component>
