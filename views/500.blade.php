@php($layoutComponent = $layoutComponent ?? \Illuminate\Support\Facades\Config::get('sm-errors.layout', 'layouts.app'))
<x-dynamic-component :component="$layoutComponent">
    <div class="exception well">
        <h3>Whoops, that didn't work!</h3>
        <div class="descr">
            Looks like whatever you were trying to do didn't work correctly.
            If it is during normal business hours, our team probably received a
            message with the error message and will be working to fix the problem
            as quickly as possible.
        </div>
    </div>
</x-dynamic-component>
