@php($layoutComponent = $layoutComponent ?? \Illuminate\Support\Facades\Config::get('sm-errors.layout', 'layouts.app'))
<x-dynamic-component :component="$layoutComponent">
    <div class="exception well">
        <h3>Sorry, you don't have access.</h3>
        <div>
            You don't have permission to access the resource you are
            trying to reach.
            @include('errors::_message', ['message' => isset($exception) ? $exception->getMessage() : null])
        </div>
    </div>
</x-dynamic-component>
