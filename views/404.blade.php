@php($layoutComponent = $layoutComponent ?? \Illuminate\Support\Facades\Config::get('sm-errors.layout', 'layouts.app'))
<x-dynamic-component :component="$layoutComponent">
    <div class="exception well">
        <h3>I don't know where to find that.</h3>
        <div class="descr">
            The resource you were trying to reach doesn't appear to exist.
            @include('errors::_message', ['message' => isset($exception) ? $exception->getMessage() : null])
        </div>
    </div>
</x-dynamic-component>
